/*
---- теория
1) forEach виконує колбек функції для масиву. тобто в цей метод передається функція, для якої можна використати три параметри, що пов'язані 
з масивом (значення в масиві, індекс та сам масив). forEach пройдеться по всім значенням масиву та виконає для них функцію

2) встановити довжину масиву 0/через splice задати значення (0, arr.length)/перезаписати масив й зробити його порожнім 

3) 1. завдяки методу Array.IsArray()  2. завдяки оператору instanceof 3. завдяки методу Object.prototype.toString.call()
*/

const arr = ['яблоко', 'банан', 1, '2', null, 'мандарин', 103, {name: 'Dan'}];

let filterBy = (arr, typeOfElem) => {
    let filterByType = arr.filter((elem) => {
        if (typeOfElem === 'null' && elem === null) {
            return false
        } 
        if (typeOfElem === 'object' && elem === null) {
            return true
        } 
        return typeof elem !== typeOfElem;
    });
    return filterByType;
};

console.log(filterBy(arr, 'null'));